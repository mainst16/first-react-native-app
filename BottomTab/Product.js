import React, { Component } from 'react'
import { StyleSheet, View,TouchableOpacity } from 'react-native'
import { Container, Header,Button ,Content,Left,Right,Body,Title ,List, ListItem, Text, Separator } from 'native-base';
import { Actions, Router, Scene} from 'react-native-router-flux'

import Icon from 'react-native-vector-icons/FontAwesome';

const Product = () => {
  return(
    
    <Container>
        <Content>
          <ListItem onPress={() =>{ Actions.Gobi();}} >
            <Text>Gobi</Text>
          </ListItem>
          <ListItem onPress={() =>{ Actions.Yama();}} >
            <Text>Yama by Gobi</Text>
          </ListItem>
          <ListItem onPress={() =>{ Actions.Organic();}} >
            <Text>Gobi Organic</Text>
          </ListItem>
          <ListItem onPress={() =>{ Actions.Kids();}}>
            <Text>Gobi Kids</Text>
          </ListItem>
        </Content>
      </Container>

  )
  
};

export default Product;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
