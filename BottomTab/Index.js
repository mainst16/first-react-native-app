import React, { Component } from 'react'
import { Text,Dimensions ,Image,StyleSheet,TouchableOpacity,ActivityIndicator,FlatList, View } from 'react-native'

import axios from 'axios'
import {Actions} from 'react-native-router-flux'

const {width,height} = Dimensions.get('window');

const numColumns = 2;
export default class Index extends Component {

    constructor(){
      super()
      this.state = {
        isLoading: true,
        dataSource: []
      }
    }

    componentDidMount()
    {
      const {item} = this.props;
      fetch('http://www.gobi.mn/api/gobi_em_palito').then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          dataSource: responseJson.products
        })
      })
     
        
    }
    _renderItem = ({item}) => (
      <TouchableOpacity onPress={ this.onPressShow.bind(this,item.product_id) }>
        <View style={styles.item}>
        <Image 
            source={{uri: 'http://www.gobi.mn/storage/' + item.pics}}
            style={{width: width / 2 -8, height: 206,borderRadius: 5}}
        />
        </View>
      </TouchableOpacity>
    );

    onPressShow(item) {
      Actions.Test({id: item });
      // alert(item);
  }
    
  render() {
    if(this.state.isLoading){
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" animating />
      </View>
    )

   } else {
     return(
      <View style={styles.container}>
      
          <FlatList data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index}
            numColumns={numColumns} style={{marginTop: 6,flex: 1,}}
      />
      </View>
      );
   }
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    item: {
      padding: 2,
      borderBottomWidth: 1,
      borderBottomColor: '#fff',
      
    }
});
