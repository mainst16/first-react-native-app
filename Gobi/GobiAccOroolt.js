import React, { Component } from 'react'
import { Text, Image,StyleSheet,TouchableOpacity,ActivityIndicator,FlatList, View } from 'react-native'

import axios from 'axios'

const numColumns = 2;
export default class Index extends Component {

    constructor(){
      super()
      this.state = {
        isLoading: true,
        dataSource: []
      }
    }

    componentDidMount()
    {
      const {item} = this.props;
      fetch('http://www.gobi.mn/api/gobi_acc_oroolt').then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          dataSource: responseJson.products
        })
      })
     
        
    }
    _renderItem = ({item}) => (
      <TouchableOpacity onPress={() => alert(item.name)}>
        <View style={styles.item}>
        <Image 
            source={{uri: 'http://www.gobi.mn/storage/' + item.pics}}
            style={{width: 175, height: 220,borderRadius: 5}}
        />
        </View>
      </TouchableOpacity>
    );
  render() {
    if(this.state.isLoading){
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" animating />
      </View>
    )

   } else {
     return(
      <View style={styles.container}>
      
          <FlatList data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index}
            numColumns={numColumns} style={{marginTop: 10,flex: 1,}}
      />
      </View>
      );
   }
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    item: {
      padding: 3,
      borderBottomWidth: 1,
      borderBottomColor: '#fff',
      
    }
});
