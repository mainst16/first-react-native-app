import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { Container, Icon,Header,Button ,Content,Left,Right,Body,Title ,List, ListItem,Separator } from 'native-base';
import { Actions, Router, Scene} from 'react-native-router-flux'

const Gobi = () => {

    return(
        <Container>
        <Content>
          <ListItem itemHeader>
            <Right>
            <Text styles={styles.header} >Эмэгтэй</Text>
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiEmPalito();}} >
            <Left>
            <Text>Пальто</Text>
            </Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiEmPidjak();}} >
            <Left><Text>Пиджак</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiEmTsamts();}} >
            <Left><Text>Цамц</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiEmOmd();}} >
            <Left><Text>Өмд</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiEmPlate();}}>
            <Left><Text>Платье</Text></Left>       
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiEmYubka();}}>
          <Left><Text>Юбка</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiEmNumrug();}}>
            <Left><Text>Нөмрөг</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem itemHeader>
            <Right>
            <Text>Эрэгтэй</Text>
            </Right>       
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiErPalito();}} >
            <Text>Пальто</Text>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiErPidjak();}} >
            <Text>Пиджак</Text>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiErTsamts();}} >
            <Text>Цамц</Text>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiErOmd();}} >
            <Text>Өмд</Text>
          </ListItem>
          <ListItem itemHeader>
            <Right>
            <Text>Аксессуар</Text>
            </Right>       
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiAccMalgai();}}>
            <Left><Text>Малгай</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiAccOroolt();}}>
            <Left><Text>Ороолт</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiAccShaali();}}>
            <Left><Text>Шааль</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiAccOims();}}>
            <Left><Text>Оймс</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiAccBeeli();}}>
            <Left><Text>Бээлий</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiAccAlchuur();}}>
            <Left><Text>Алчуур</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem itemHeader>
            <Right>
            <Text>Гэр ахуй</Text>
            </Right>       
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiGerHunjil();}}>
            <Left><Text>Хөнжил</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiGerNumrug();}}>
            <Left><Text>Нөмрөг</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiGerNumrug();}}>
            <Left><Text>Бүтээлэг</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiGerNumrug();}}>
            <Left><Text>Дэр</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiGerNumrug();}}>
            <Left><Text>Орхимж</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiGerNumrug();}}>
            <Left><Text>Аяны хөнжил</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem onPress={() =>{ Actions.GobiGerNumrug();}}>
            <Left><Text>Аяны сет</Text></Left>
            <Right>
            <Icon name="arrow-forward" />
            </Right>
          </ListItem>
        </Content>
      </Container>
    )
};

export default Gobi;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ddd',
        alignItems: 'center',
        justifyContent: 'center',
      },
    header: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',

    }
})

