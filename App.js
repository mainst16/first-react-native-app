import React from 'react';
import { StyleSheet,View ,Button,Text, SafeAreaView } from 'react-native';

import { createStackNavigator, createAppContainer } from 'react-navigation';

import { Container,Left,Title,Right ,Body,Header,Content, Footer, FooterTab } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Router,Stack,Actions,Scene} from 'react-native-router-flux'


import Index from './BottomTab/Index'
import Product from './BottomTab/Product'
import Notfication from './BottomTab/Notfication'
import Account from './BottomTab/Account'

import Now from './Now/Now'

import Yama from './Brand/Yama'
import Organic from './Brand/Organic'
import Gobi from './Brand/Gobi'
import Kids from './Brand/Kids'

import GobiEmPalito from './Gobi/GobiEmPalito'
import GobiEmPidjak from './Gobi/GobiEmPidjak'
import GobiEmTsamts from './Gobi/GobiEmTsamts'
import GobiEmOmd from './Gobi/GobiEmOmd'
import GobiEmPlate from './Gobi/GobiEmPlate'
import GobiEmNumrug from './Gobi/GobiEmNumrug'
import GobiEmYubka from './Gobi/GobiEmYubka'

import GobiErPalito from './Gobi/GobiErPalito'
import GobiErPidjak from './Gobi/GobiErPidjak'
import GobiErTsamts from './Gobi/GobiErTsamts'
import GobiErOmd from './Gobi/GobiErOmd'

import GobiGerHunjil from './Gobi/GobiGerHunjil'

import GobiAccMalgai from './Gobi/GobiAccMalgai'
import GobiAccOroolt from './Gobi/GobiAccOroolt'
import GobiAccShaali from './Gobi/GobiAccShaali'
import GobiAccOims from './Gobi/GobiAccOims'
import GobiAccBeeli from './Gobi/GobiAccBeeli'
import GobiAccAlchuur from './Gobi/GobiAccAlchuur'

import Test from './Gobi/Test'
import Flex from './Gobi/Flex'
import NowItem from './Now/NowItem'

const TabIcon = ({focused, iconName}) => {
  var color = focused ? '#34495e' : '#7f8c8d';
    return(
      <Icon name={iconName} color={color} size={25} />
    );
};

const App = () => {
  return(
  <Router>
    
    <Scene key="root">

        <Scene
          key="tabbar"
          showLabel={false}
          tabs
          hideNavBar
          >
{/* Tab 1 */}
          <Scene
            key="tab1"
            icon={TabIcon}
            iconName="home">

            <Scene
              key="Index"
              component={Index}
    
            />
          </Scene>

{/* Tab 2 */}
          <Scene
            key="tab2"
            icon={TabIcon}
            iconName="shopping-bag"

               >

            <Scene
              key="Product"
              leftButtonIconStyle = {{ tintColor:'red'}}
              component={Product}
          
            />

            <Scene 
              key="Yama"
              title="Yama"
              component={Yama}
            />

            <Scene 
              key="Organic"
              title="Organic"
              component={Organic}
            />

            <Scene 
              key="Gobi"
              title="Gobi"
              component={Gobi}
            />

            <Scene 
              key="Kids"
              title="Kids"
              component={Kids}
            />

            <Scene 
              key="GobiEmPalito"
              title="Gobi"
              component={GobiEmPalito}
            />

            <Scene 
              key="GobiEmPidjak"
              title="Gobi"
              component={GobiEmPidjak}
            />

            <Scene 
              key="GobiEmTsamts"
              title="Gobi"
              component={GobiEmTsamts}
            />

            <Scene 
              key="GobiEmOmd"
              title="Gobi"
              component={GobiEmOmd}
            />
            <Scene 
              key="GobiEmPlate"
              title="Gobi"
              component={GobiEmPlate}
            />

            <Scene 
              key="GobiEmYubka"
              title="Gobi"
              component={GobiEmYubka}
            />
            
            <Scene 
              key="GobiEmNumrug"
              title="Gobi"
              component={GobiEmNumrug}
            />

            <Scene 
              key="GobiErPalito"
              title="Gobi"
              component={GobiErPalito}
            />

            <Scene 
              key="GobiErPidjak"
              title="Gobi"
              component={GobiErPidjak}
            />

            <Scene 
              key="GobiErTsamts"
              title="Gobi"
              component={GobiErTsamts}
            />

            <Scene 
              key="GobiErOmd"
              title="Gobi"
              component={GobiErOmd}
            />

             <Scene 
              key="GobiGerHunjil"
              title="Gobi"
              component={GobiGerHunjil}
            />

              <Scene 
              key="GobiAccMalgai"
              title="Gobi"
              component={GobiAccMalgai}
            />

              <Scene 
              key="GobiAccOroolt"
              title="Gobi"
              component={GobiAccOroolt}
            />

              <Scene 
              key="GobiAccShaali"
              title="Gobi"
              component={GobiAccShaali}
            />

              <Scene 
              key="GobiAccOims"
              title="Gobi"
              component={GobiAccOims}
            />

              <Scene 
              key="GobiAccBeeli"
              title="Gobi"
              component={GobiAccBeeli}
            />

             <Scene 
              key="GobiAccAlchuur"
              title="Gobi"
              component={GobiAccAlchuur}
            />

            <Scene
            key="Test"
            title="Gobi"
            component={Test}
            />
            
            <Scene
              key="Flex"
              title="Flex"
              component={Flex}
            />
            
          </Scene>

      
{/* Tab 3 */}
          <Scene
            key="tab3"
            icon={TabIcon}
            iconName="bandcamp"

               >

            <Scene
              key="Now"
              title="Now News"
              component={Now}
          
            />
            <Scene
              key="NowItem"
              title="Now Item"
              component={NowItem}
            />

          </Scene>
{/* Tab 4 */}
          <Scene
            key="tab4"
            title="hehe"
            icon={TabIcon}
            iconName="user"

               >

            <Scene
              key="Account"
              title="Тохиргоо"
              component={Account}
              hideNavBar
              
          
            />

          </Scene>
          

        </Scene>

    </Scene>
  </Router>
  );
};
export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
