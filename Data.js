const URI = 'https://randomuser.me/api/?results=10&inc=name,registered&nat=fr';

export default {
    async fetchData(){
        try{
            let response = await fetch(URI);
            let responseJsonData = await response.json();
            return responseJsonData;
        }
    catch(error){
        console.log(error)
    }
    }
}